import mysql.connector

class Connection():
    @staticmethod
    def create_connection():
        try:
            mydb = mysql.connector.connect(
                host="localhost",
                user="root",
                password=""
            )
            
            mycursor = mydb.cursor()
            mycursor.execute("CREATE DATABASE IF NOT EXISTS manajemen_proyek")
        except Exception as error:
            print("Error while connecting to database", error)
