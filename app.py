from ui import Ui
from function import Function
from data import Data
from connection.conn import Connection
import pyinputplus as pyip
import traceback

class App:
    def start(self):
        try:
            Connection().create_connection()
            while True:
                # region list menu
                Ui.head()
                menu = pyip.inputNum(prompt="Pilih Menu?: ", min = 0)
                choice = Function().Choice(menu)
                print(choice)
                Ui.separator()

                # region action menu
                Function().Action(menu)
                Ui.separator()
                
                # region opsi lanjut/tidak
                repeat = input("Lanjutkan? (y/n): ")
                if repeat == "y" or choice == "Y":
                    continue
                elif repeat == "n" or choice == "N":
                    print("Terimakasih telah menggunakan program kami, sampai jumpa!")
                    break
                else:
                    print("Input tidak valid")
                    next = input("Lanjutkan? (y/n): " + "\n")
                    repeat = Function().Continue(next)
        except Exception as e:
            print('Terjadi Kesalahan: ' + str(e))
            print(''.join(traceback.TracebackException.from_exception(e).format()))

App().start()
