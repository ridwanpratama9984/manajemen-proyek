from data import Data
from proyek import Proyek
import pyinputplus as pyip
from ui import Ui

class Function:
    def Choice(self, choice):
        choice = int(choice)

        validChoice = [1, 2, 3]
        if choice not in validChoice:
            return ("Pilihan tidak tersedia")

        if choice == 1:
            return ("Menu yang dipilih: List Proyek")
        elif choice == 2:
            return ("Menu yang dipilih: Tambah Proyek")
        elif choice == 3:
            return ("Menu yang dipilih: Pilih Proyek")

    def Action(self, choice):
        choice = int(choice)
        if choice == 1:
            data = Proyek().ListProyek()
            print("List Proyek: " + "\n")
            for item in data:
                print("ID: {}".format(item['id']))
                print("Name: {}".format(item['name']))
                print("Desc: {}".format(item['desc']))
                print("Year: {}\n".format(item['year']))
        elif choice == 2:
            print("Tambah Proyek")
        elif choice == 3:
            noproyek = pyip.inputNum(prompt="Pilih Proyek?: ", min = 0)
            Ui.separator()
            selectedProyek = Proyek().pilihProyek(noproyek)
            print("Proyek yang dipilih: ")
            print("ID: {}".format(selectedProyek['id']))
            print("Name: {}".format(selectedProyek['name']))
            print("Desc: {}".format(selectedProyek['desc']))
            print("Year: {}".format(selectedProyek['year']))
            
            Ui.separator()
            Ui.proyekMenu()
            selectedMenu = pyip.inputNum(prompt="Pilih Menu?: ", min = 0)
            

    def Continue(self, choice):
        if choice == "y" or choice == "Y":
            return (1)
        elif choice == "n" or choice == "N":
            return (2)
        else:
            return (3)
