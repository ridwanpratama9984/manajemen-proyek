class Data:
    def Proyek(self):
        listProyek = [
            {
                "id": "1",
                "name": "Proyek Satu",
                "desc": "Deskripsi proyek satu",
                "year": 2023,
            },
            {
                "id": "2",
                "name": "Proyek Dua",
                "desc": "Deskripsi proyek dua",
                "year": 2023,
            },
            {
                "id": "3",
                "name": "Proyek Tiga",
                "desc": "Deskripsi proyek Tiga",
                "year": 2023,
            }
        ]

        return (listProyek)
